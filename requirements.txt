Flask==1.1.2
Flask-RESTful==0.3.8
Flask-SQLAlchemy==2.4.3
Jinja2==2.11.2
psycopg2==2.8.5
