# Flask RESTfulApi 
This is a very simple project done using flask. It's a RESTful API where
guest can be created and do a reservation for a particular room.

# Dependencies

- It is required you to have these dependencies:

- Flask==1.1.2
- Flask-RESTful==0.3.8
- Flask-SQLAlchemy==2.4.3
- Jinja2==2.11.2
- psycopg2==2.8.5



# Getting Started

Clone The Project `https://gitlab.com/felmazi/flask-restfulapi.git`

# Configure the Database
- Install the dependencies
- Create a database in PosrgreSQL

```
##  app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://freskim:password@localhost:5432/hotel_db'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db = SQLAlchemy(app)
    migrate = Migrate(app, db)
```
- Run these commands to create the tables

```
$ flask db stamp head
$ flask db migrate
$ flask db upgrade
```

- Run the application

# Endpoints

-For Room Entity

`http://localhost:5000/rooms` (GET) to get all the rooms

`http://localhost:5000/rooms/<room_id>` (GET) to get a room by id

`http://localhost:5000/rooms` (POST) to create a new room

`http://localhost:5000/rooms/<room_id>` (PUT) to update an existing room

`http://localhost:5000/rooms/<room_id>` (DELETE) to delete an existing room


-For Employee Entity

`http://localhost:5000/employees` (GET) to get all the employees

`http://localhost:5000/employees/<employee_id>` (GET) to get an employee by id

`http://localhost:5000/employees` (POST) to create a new employee

`http://localhost:5000/employees/<employee_id>` (PUT) to update an existing employee

`http://localhost:5000/employees/<employee_id>` (DELETE) to delete an existing employee


-For Guest Entity

`http://localhost:5000/guests` (GET) to get all the guests

`http://localhost:5000/guests/<guest_id>` (GET) to get a guest by id

`http://localhost:5000/guests` (POST) to create a new guest

`http://localhost:5000/guests/<guest_id>` (PUT) to update an existing guest

`http://localhost:5000/guests/<guest_id>` (DELETE) to delete an existing guest


-For Reservation Entity

`http://localhost:5000/reservations` (GET) to get all the reservations

`http://localhost:5000/reservations/<reservation_id>` (GET) to get a reservation by id

`http://localhost:5000/reservations` (POST) to create a new reservation

`http://localhost:5000/reservations/<reservation_id>` (PUT) to update an existing reservation

`http://localhost:5000/reservations/<reservation_id>` (DELETE) to delete an existing reservation
