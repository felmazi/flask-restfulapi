from flask_restful import Api
from init.app import app
from api.controllers.employees import EmployeeResource, EmployeeResourceList
from api.controllers.guests import GuestResource, GuestResourceList
from api.controllers.rooms import RoomResource, RoomResourceList
from api.controllers.reservations import ReservationResource, ReservationResourceList


api = Api(app)
api.add_resource(EmployeeResource, '/employees')
api.add_resource(EmployeeResourceList, '/employees/<employee_id>')

api.add_resource(GuestResource, '/guests')
api.add_resource(GuestResourceList, '/guests/<guest_id>')

api.add_resource(RoomResource, '/rooms')
api.add_resource(RoomResourceList, '/rooms/<room_id>')

api.add_resource(ReservationResource, '/reservations')
api.add_resource(ReservationResourceList, '/reservations/<reservation_id>')

if __name__ == '__main__':
    app.run(debug=True)
