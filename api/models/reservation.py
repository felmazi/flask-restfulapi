from init.app import db
from api.models.guest import Guest
from api.models.room import Room


class Reservation(db.Model):
    __tablename__ = 'reservations'

    id = db.Column(db.Integer, primary_key=True)
    price = db.Column(db.Integer, nullable=False)
    start_date = db.Column(db.String(80), nullable=False)
    end_date = db.Column(db.String(80), nullable=False)
    room_id = db.Column(db.Integer, db.ForeignKey(Room.id))
    guest_id = db.Column(db.Integer, db.ForeignKey(Guest.id))

    @property
    def serialize(self):
        """Return object data in easily serializable format"""
        return {
            'id': self.id,
            'price': self.price,
            'start_date': self.start_date,
            'end_date': self.end_date,
            'room_id': self.room_id,
            'guest_id': self.guest_id

        }

    def __init__(self, price, start_date, end_date, room_id, guest_id):
        self.price = price
        self.start_date = start_date
        self.end_date = end_date
        self.room_id = room_id
        self.guest_id = guest_id


    def __repr__(self):
        return '<Reservation {}>'.format(self.id)

