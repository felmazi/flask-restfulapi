from init.app import db


class Guest(db.Model):
    __tablename__ = 'guests'

    id = db.Column(db.Integer, primary_key=True)
    full_name = db.Column(db.String(80), nullable=False)
    email = db.Column(db.String(80), unique=True, nullable=False)
    personal_id = db.Column(db.String(80), nullable=False)
    country = db.Column(db.String(80), nullable=False)
    city = db.Column(db.String(80), nullable=False)
    address = db.Column(db.String(80), nullable=False)
    phone = db.Column(db.String(80), nullable=False)

    @property
    def serialize(self):
        """Return object data in easily serializable format"""
        return {
            'id': self.id,
            'full_name': self.full_name,
            'email': self.email,
            'personal_id': self.personal_id,
            'country': self.country,
            'city': self.city,
            'address': self.adress,
            'phone': self.phone
        }

    def __init__(self, full_name, email, personal_id, country, city, address, phone):
        self.full_name = full_name
        self.email = email
        self.personal_id = personal_id
        self.country = country
        self.city = city
        self.address = address
        self.phone = phone

    def __repr__(self):
        return '<Guest {}>'.format(self.id)

