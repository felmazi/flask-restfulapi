from init.app import db


class Room(db.Model):
    __tablename__ = 'rooms'

    id = db.Column(db.Integer, primary_key=True)
    floor = db.Column(db.Integer, nullable=False)
    is_available = db.Column(db.Boolean, nullable=False)
    people = db.Column(db.Integer, nullable=False)
    room_number = db.Column(db.Integer, unique=True, nullable=False)
    price = db.Column(db.Integer, nullable=False)


    @property
    def serialize(self):
        """Return object data in easily serializable format"""
        return {
            'id': self.id,
            'floor': self.floor,
            'is_available': self.is_available,
            'people': self.people,
            'room_number': self.room_number,
            'price': self.price
        }

    def __init__(self, floor, is_available, people, room_number, price):
        self.floor = floor
        self.is_available = is_available
        self.people = people
        self.room_number = room_number
        self.price = price

    def __repr__(self):
        return '<Room {}>'.format(self.id)

