from flask import Flask, request
from flask_restful import Resource, reqparse
from init.app import db
from api.models.reservation import Reservation

parser = reqparse.RequestParser()
parser.add_argument('id')

class ReservationResource(Resource):
    def get(self):
        reservations = Reservation.query.all()
        results = [
            {
                'id': reservation.id,
                'price': reservation.price,
                'start_date': reservation.start_date,
                'end_date': reservation.end_date,
                'room_id': reservation.room_id,
                'guest_id': reservation.guest_id
            } for reservation in reservations]

        return {"count": len(results), "employees": results}

    def post(self):
        if request.is_json:
            data = request.get_json()
            new_reservation = Reservation(price=data['price'], start_date=data['start_date'], end_date=data['end_date'], room_id=data['room_id'], guest_id=data['guest_id'])
            db.session.add(new_reservation)
            db.session.commit()
            return {"message": "Reservation " + str(new_reservation.id) + " has been created successfully."}
        else:
            return {"error": "The request payload is not in JSON format"}

class ReservationResourceList(Resource):
    def put(self, reservation_id):
        reservation = Reservation.query.get_or_404(reservation_id)

        data = request.get_json()
        reservation.price=data['price']
        reservation.start_date=data['start_date']
        reservation.end_date=data['end_date']
        reservation.room_id=data['room_id']
        reservation.guest_id=data['guest_id']
        db.session.add(reservation)
        db.session.commit()
        return {"message": "Reservation " + str(reservation.id) + " successfully updated"}

    def get(self, reservation_id):
        reservation = Reservation.query.get_or_404(reservation_id)

        response = {
            'id': reservation.id,
            'price': reservation.price,
            'start_date': reservation.start_date,
            'end_date': reservation.end_date,
            'room_id': reservation.room_id,
            'guest_id': reservation.guest_id
        }
        return {"message": "success", "reservation": response}

    def delete(self, reservation_id):
        reservation = Reservation.query.get_or_404(reservation_id)

        db.session.delete(reservation)
        db.session.commit()
        return {"message": "Reservation " + str(reservation.id) + " successfully deleted."}