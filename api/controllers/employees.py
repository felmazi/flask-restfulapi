from flask import Flask, request
from flask_restful import Resource, reqparse
from init.app import db
from api.models.employee import Employee

parser = reqparse.RequestParser()
parser.add_argument('full_name')

class EmployeeResource(Resource):
    def get(self):
        employees = Employee.query.all()
        results = [
            {
                "id": employee.id,
                "full_name": employee.full_name,
                "email": employee.email
            } for employee in employees]

        return {"count": len(results), "employees": results}

    def post(self):
        if request.is_json:
            data = request.get_json()
            new_employee = Employee(full_name=data['full_name'], email=data['email'], password=data['password'])
            db.session.add(new_employee)
            db.session.commit()
            return {"message": "Employee " + new_employee.full_name + " has been created successfully."}
        else:
            return {"error": "The request payload is not in JSON format"}

class EmployeeResourceList(Resource):
    def put(self, employee_id):
        employee = Employee.query.get_or_404(employee_id)
        data = request.get_json()
        employee.full_name = data['full_name']
        employee.email = data['email']
        employee.password = data['password']
        db.session.add(employee)
        db.session.commit()
        return {"message": "Employee " + employee.full_name + " successfully updated"}

    def get(self, employee_id):
        employee = Employee.query.get_or_404(employee_id)

        response = {
            "id": employee.id,
            "full_name": employee.full_name,
            "email": employee.email
        }
        return {"message": "success", "employee": response}

    def delete(self, employee_id):
        employee = Employee.query.get_or_404(employee_id)
        db.session.delete(employee)
        db.session.commit()
        return {"message": "Employee " + employee.full_name + " successfully deleted."}