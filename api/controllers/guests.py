from flask import Flask, request
from flask_restful import Resource, reqparse
from init.app import db
from api.models.guest import Guest

parser = reqparse.RequestParser()
parser.add_argument('full_name')

class GuestResource(Resource):
    def get(self):
        guests = Guest.query.all()
        results = [
            {
                'id': guest.id,
                'full_name': guest.full_name,
                'email': guest.email,
                'personal_id': guest.personal_id,
                'country': guest.country,
                'city': guest.city,
                'address': guest.address,
                'phone': guest.phone
            } for guest in guests]

        return {"count": len(results), "guests": results}

    def post(self):
        if request.is_json:
            data = request.get_json()
            new_guest = Guest(full_name=data['full_name'], email=data['email'], personal_id=data['personal_id'], country=data['country'], city=data['city'], address=data['address'], phone=data['phone'])
            db.session.add(new_guest)
            db.session.commit()
            return {"message": "Guest " + new_guest.full_name + " has been created successfully."}
        else:
            return {"error": "The request payload is not in JSON format"}

class GuestResourceList(Resource):
    def put(self, guest_id):
        guest = Guest.query.get_or_404(guest_id)

        data = request.get_json()
        guest.full_name = data['full_name']
        guest.email = data['email']
        guest.personal_id=data['personal_id']
        guest.country=data['country']
        guest.city=data['city']
        guest.address=data['address']
        guest.phone=data['phone']
        db.session.add(guest)
        db.session.commit()
        return {"message": "Guest " + guest.full_name + " successfully updated"}

    def get(self, guest_id):
        guest = Guest.query.get_or_404(guest_id)

        response = {
            'id': guest.id,
            'full_name': guest.full_name,
            'email': guest.email,
            'personal_id': guest.personal_id,
            'country': guest.country,
            'city': guest.city,
            'address': guest.address,
            'phone': guest.phone
        }
        return {"message": "success", "guest": response}

    def delete(self, guest_id):
        guest = Guest.query.get_or_404(guest_id)
        db.session.delete(guest)
        db.session.commit()
        return {"message": "Guest " + guest.full_name + " successfully deleted."}