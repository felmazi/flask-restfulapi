from flask import Flask, request
from flask_restful import Resource, reqparse
from init.app import db
from api.models.room import Room

parser = reqparse.RequestParser()
parser.add_argument('room_number')

class RoomResource(Resource):
    def get(self):
        rooms = Room.query.all()
        results = [
            {
                'id': room.id,
                'floor': room.floor,
                'is_available': room.is_available,
                'people': room.people,
                'room_number': room.room_number,
                'price': room.price
            } for room in rooms]

        return {"count": len(results), "rooms": results}

    def post(self):
        if request.is_json:
            data = request.get_json()
            new_room = Room(floor=data['floor'], is_available=data['is_available'], people=data['people'], room_number=data['room_number'], price=data['price'])
            db.session.add(new_room)
            db.session.commit()
            return {"message": "Room " + str(new_room.room_number) + " has been created successfully."}
        else:
            return {"error": "The request payload is not in JSON format"}

class RoomResourceList(Resource):
    def put(self, room_id):
        room = Room.query.get_or_404(room_id)

        data = request.get_json()
        room.floor=data['floor']
        room.is_available=data['is_available']
        room.people=data['people']
        room.room_number=data['room_number']
        room.price=data['price']
        db.session.add(room)
        db.session.commit()
        return {"message": "Room " + str(room.room_number) + " successfully updated"}

    def get(self, room_id):
        room = Room.query.get_or_404(room_id)

        response = {
            'id': room.id,
            'floor': room.floor,
            'is_available': room.is_available,
            'people': room.people,
            'room_number': room.room_number,
            'price': room.price
        }
        return {"message": "success", "room": response}

    def delete(self, room_id):
        room = Room.query.get_or_404(room_id)

        db.session.delete(room)
        db.session.commit()
        return {"message": "Room " + str(room.room_number) + " successfully deleted."}